![Application](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/application_screenshot.png)

# Wege aus den Irrgärten

Projektarbeit von Lewin Gerber, Mischa Maurer und Oliver Thoma <br/>
Betreut durch Guido Trommsdorff und Marinko Veselcic <br/>
Kantonsschule Büelrain Winterthur <br/>
Frühlingssemester 2021 <br/>

## Inhaltsverzeichnis

1. [Problembeschreibung](#problembeschreibung)
    1. [Einsatz in der Praxis](#einsatz-in-der-praxis)
    2. [Problemanalyse](#problemanalyse)
2. [Irrgarten erstellung](#maze-generation)
   1. [Zusammenhängend](#zusammenhängend)
   2. [Nicht Zusammenhängend](#nicht-zusammenhängend)
3. [Algorithmen für Wege aus den Irrgärten](#lösungsansätze)
    1. [Rechte-Hand Regel](#rechte-hand)
        1. [Erläuterung](#erläuterung-2)
        2. [Vor- und Nachteile](#vor-und-nachteile-2)
        3. [Umsetzung](#umsetzung-2)
    2. [Pledge-Algorithmus](#pledge)
        1. [Erläuterung](#erläuterung-1)
        2. [Vor- und Nachteile](#vor-und-nachteile-1)
        3. [Umsetzung](#umsetzung-1)
4. [Visualisierung](#visualisierung)
5. [Endresultat](#endresultat)
    1. [Funktionsweise](#funktionsweise)
        1. [Gamification](#gamification)
    2. [Grenzen und Probleme](#grenzen)
6. [Reflexion](#reflexion)

<a name="problembeschreibung"></a>

## Problembeschreibung

In dieser Projektarbeit wird eine zweiteilige Problemstellung behandelt. Einerseits geht es darum einen
Irrgarten zu erstellen und danach darum für diesen einen Weg von einem bestimmten Anfangspunkt zu einem Endpunkt zu finden. Im Rahmen dieser Projektarbeit wird dieses Problem in einer Applikation vereint. Das folgende Dokument beschreibt die Logik hinter den Lösungen und welche Probleme dabei gelöst werden müssen.

Als erste Erkenntnis kann festgestellt werden, dass es verschiedene Irrgärten gibt. Auf der einen Seite wäre dies ein perfekter und somit zusammenhängender Irrgarten und auf der anderen wäre dies ein nicht perfekter und somit nicht vollumfänglich zusammenhängender Irrgarten. Bei der Erstellung müssen diese Typen klar unterschieden werden und haben jeweils ihre eigenen Herausforderungen. 
Die perfekten Irrgärten stellen ein kleines Problem dar, da selbst ein einfacher Algorithmus, wie die "Rechte-Hand-Regel", für diese einen Weg findet. Bei nicht perfekten und komplexeren ist dies allerdings nicht mehr möglich und der einfachste Algorithmus zur Lösung wäre der Pledge-Algorithmus.
Bevor aber beschrieben wird wie genau dies gelöst werden kann, wird beschrieben, wo die Wegfindung (engl. Pathfinding) ihr Anwendung hat.


<a name="einsatz-in-der-praxis"></a>

### Einsatz in der Praxis

Irrgärten zu lösen findet den Bezug zur Praxis über die grundlegende Idee der Wegfindung. Das Lösen ist vor allem für Simulationen wie bei Videospielen von grosser Bedeutung ([Artikel](https://arrow.tudublin.ie/cgi/viewcontent.cgi?article=1063&context=itbj)). Nur so kann eine computergesteuerte Charakter sein Ziel finden und sich zielorientiert bewegen. Es wird dabei immer versucht von einem Startpunkt zu einem Endpunkt zu gelangen und dies mit einer möglichst effizienten Methode. Effizient meint dabei, dass der Weg möglichst kurz sein soll und andererseits effizient in der aufgewendeten Zeit zum Finden des Weges. 


<a name="problemanalyse"></a>

### Problemanalyse

In diesem Abschnitt wird kurz erklärt welche Problemstellungen beim Erstellen und Wegfinden bei Irrgärten entsteht. 
Das Problem der Erstellung besteht darin, dass es möglich sein muss einen zufälligen Irrgarten zu erstellen, aber gleichzeitig die Eigenschaften eines Irrgartens gezielt umzusetzen. Es handelt sich also um eine zufällige Erstellung mit Einschränkungen.

Das Problem der Wegfindung besteht darin, dass nicht alle Algorithmen alle Irrgärten lösen können. Um zu demonstrieren, wieso dies so ist, wird zuerst aufgezeigt wie die Rechte-Hand-Regel an das Problem herangeht und wie diese dann scheitert. Als Ergänzung dazu kann der Pledge-Algorithmus verwendet werden. Dieser ist ebenfalls ein relativ unkomplexer Algorithmus, welcher allerdings sowohl für zusammenhängende wie auch nicht zusammenhängende Irrgärten einen Weg findet.
Im Rahmen der Wegfindung beschränkt sich diese aber ausschliesslich auf das Finden und nicht auf das Optimieren eines Weges.

<a name="maze-generation"></a>

## Irrgarten Erstellung

In diesem Kapitel wird erklärt wie diese Applikation einen zusammenhängenden Irrgarten erstellt und was die Eigenschaften von verschiedenen Irrgärtenarten sind. Heutzutage werden die Begriffe Labyrinth und Irrgarten oft als dasselbe verwendet. Historisch gesehen gibt es allerdings einen klaren Unterschied. So ist ein Labyrinth dadurch definiert, dass es nur einen korrekten Weg gibt und dieser abgelaufen werden muss. Das typische Bild eines Labyrinths ist ebenfalls davon geprägt, dass es das Ziel ist das Zentrum zu erreichen. Anders aber der Irrgarten, wo es viele Sackgassen und verwinkelte Abzweigungen gibt. Es gibt ganz klar einen optimalen Weg, aber in diesem Fall ist es nicht nötig diese zu gehen um vom Startpunkt zum Endpunkt zu gelangen. Diese [Liste](https://www.diffen.com/difference/Labyrinth_vs_Maze) zeigt weitere Unterschiede auf. Der folgende Prozess beschreibt nun die Erstellung eines Irrgartens.

<a name="zusammenhängend"></a>

### Zusammenhängend
<div align="center">
![zusammenhängend](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/zusammenhängender_irrgarten.png)
<div style="font-style: italic">
    Die Abbildung oberhalb zeigt ein zusammenhängender Irrgarten.
</div>
</div>
<br />

Um den die Idee hinter dem Algorithmus zu verstehen, müssen die Merkmale eines Irrgartens bestimmt werden. In diesem Fall werden die 4 Grundmerkmale eines Irrgartens aufgelistet. Diese sind alle zwingend zu beachten, wenn ein Irrgarten erstellt wird:

1. er besteht aus **zusammenhängenden Wänden und Wegen**
2. **jedes Feld** ist mit jedem anderen Feld **verbunden**
3. es gibt **einen Ein- und einen Ausgang**
4. Weg und Wand haben eine **Breite von 1er Einheit**

Obwohl alle diese Punkte für das Erstellen alle nicht weggelassen werden können unterscheiden sie sich in ihrer Schwierigkeit bei der Umsezung. Der oberste Punkt mit den zusammenhängenden Wegen und Wänden stellt dabei die grösste Schwierigkeit dar, da ein Irrgarten immer zufällig erstellt wird diese Bedingung aber erfordert, dass der Zufall teils kontrolliert wird. Es ist nicht schwer zu erkennen, dass das zufällige freiräumen von Feldern in einem Irrgarten nicht zu einem zusammenhängenden Resultat führen wird. Und genau an dieser Stelle beginnt der interessante Teil dieser Lösun, denn hier verbinden sich die beiden Problemstellungen der Projektarbeit auf einer abstrakten Ebene. Denn die Lösung des zusammenhängenden Irrgartens kann mit der Hilfe von einer Art Pathfinding (so wie beispielsweise dem Pledge-Algorithmus) gelöst werden kann. Dieser baut sich dann wie beim Lösen des Irrgartens ein Weg durch den Irrgarten. Die Idee dahinter ist, dass wenn man sich von einem Punkt aus nach einem Zufallsprinzip nach aussenbaut, dass alle Teile zwingend zusammenhängend sein müssen. Angenommen ein solcher Weg ist entstanden, dann wären die ersten drei Anforderungen erfüllt. Denn alle Wege wären zusammenhängend, jedes Feld wäre mit jedem anderen verbunden, da es ein langer zusammenhängender Weg ist, und ein Eingang könnte überall dort platziert werden wo es am Rand keine Wand hat.

In der Praxis müssen zur Umsetzung dieses Lösungsansatzes verschiedene Schritte befolgt werden. Als Voraussetzung braucht es einen bereits erstellen Irrgarten, welcher nur aus Wänden besteht. Der Pathfiding-Algorithmus geht dabei wie folgt vor:
Ein zufälliger Punkt ausgewählt und von diesem aus die umgebenden Punkte analysiert (geschaut ob es eine Wand ist oder nicht). Alle umliegenden Wände werden dann zwischen gespeichert, weil bei allen diesen ein weiterer Weg angesetzt wird. Aus den verschiedenen Möglichkeiten wird dann ein Teil der Wand ausgesucht und dort der Weg erweitert. Bei der Erweiterung wird zuerst das momentane Wand stück entfernt und dann geht man zusätzlich noch ein Feld nach vorne, damit auch die letzte Bedingung erfüllt wird, welche besagt, dass die Wände immer genau eine Breite von 1er Einheit haben sollen. Anschliessend wird dann von diesem Feld aus analysiert ob es Wände rundherum hat oder nicht und alle Wände werden zwischengespeichert. Diese Schritte können auch so dargestellt werden.

1. ein zufälliges Feld auswählen
2. alle umliegenden Wände werden gespeichert
3. Einer dieser Wände wird ausgewählt und ein nächstes umliegendes Feld gespeichert
4. Von diesem Feld werden dann die Wände wieder gespeichert
5. Solange es gespeicherten Wände gibt wird für diese der Weg erweitert

Wenn ein Feld analysiert wird, dann wird immer eine sehr spezifische Folge von Abfrage ausgeführt:

1. Wenn Links frei ist, wird nach Rechts erweitert
2. Wenn Rechts frei ist, wird nach Links erweitert
3. Wenn Oben frei ist, wird nach Unten erweitert
4. Wenn unten frei ist, wird nach Oben erweitert

Es wird also immer in die entgegengesetzte Richtung um ein Feld erweitert (damit die Wand eine Breite von 1er Einheit hat). Dies führt dazu, dass die Wege zwingend zusammenhängend sind und so jeder Punkt mit jedem anderen verbunden wird. Diese Aufzählungen können visuell so dargestellt werden:

<div align="center">
    <img align="center" width="500px" src="https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/am_maze_generation.gif">
</div>

Die Implementierung dieser Idee oberhalb, sieht dann in JavaScript so aus:

````javascript
let {
      width,
      height,
      empty: EMPTY = 0,
      wall: WALL = 1,
    } = options;

    // empty
    const OOB = {};


    // array mit Wänden füllen
    const out = new Array(height);
    for (let y = 0; y < out.length; y++) {
        out[y] = new Array(width).fill(WALL);
    }

    // nachsehen ob ein Feld eine Wand oder ein Weg ist
    const lookup = (field, x, y, defaultValue = EMPTY) => {
        if (x < 0 || y < 0 || x >= width || y >= height)
            return defaultValue;
        return field[y][x];
    }

    // mögliche Punkte um den Weg weiterzuführen
    const walls = [];

    const makePassage = (x, y) => {
        out[y][x] = EMPTY;

        // Punkte um das Feld herum
        const candidates = [
            { x: x - 1, y },
            { x: x + 1, y },
            { x, y: y - 1 },
            { x, y: y + 1 },
        ];

        // neue Punkte um das Feld zum "walls" Array hinzufügen
        candidates.forEach((wall) => {
            if (lookup(out, wall.x, wall.y) === WALL) walls.push(wall);
        });
    }

    // zufällige Position auswählen um den Weg fortzuführen
    makePassage(Math.random() * width | 0, Math.random() * height | 0);

    while (walls.length !== 0) {
        const { x, y } = walls.splice((Math.random() * walls.length) | 0, 1)[0];

        // bestimmen was die Felder um das momentane Feld sind
        const left = lookup(out, x - 1, y, OOB);
        const right = lookup(out, x + 1, y, OOB);
        const top = lookup(out, x, y - 1, OOB);
        const bottom = lookup(out, x, y + 1, OOB);

        // herausfinden in welche Richtung der Weg erweitert wird
        if (left === EMPTY && right === WALL) {
            out[y][x] = EMPTY;
            makePassage(x + 1, y);
        } else if (right === EMPTY && left === WALL) {
            out[y][x] = EMPTY;
            makePassage(x - 1, y);
        } else if (top === EMPTY && bottom === WALL) {
            out[y][x] = EMPTY;
            makePassage(x, y + 1);
        } else if (bottom === EMPTY && top === WALL) {
            out[y][x] = EMPTY;
            makePassage(x, y - 1);
        }
    }
````

Ein Irrgarten kann abstrahiert werden indem dieser als Baum dargestellt wird. Auf der Abbildung unterhalb wird das Beispiel eines zusammenhängenden Irrgartens nochmals gezeigt, aber diesmal ist auf allen Wegen und Kreuzungen der Baum mit wegen und Aufteilungen eingezeichnet. Eine solche Form des Irrgartens könnte angewendet werden um eine einheitliche Form des Irrgratens für das Einsetzen von verchiedenen Algorithmen zu schaffen. So ist das Erstellen komplett unabhängig von den Algorithmen.

Baum im Irrgarten          | Isolierter Baum
:-------------------------:|:-------------------------:
![Baum PNG](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/zusammenhängend_baum.png)  |  ![Baum SVG](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/zusammenhängender_baum_only.png)

Der beschriebene Ansatz folgt den Grundprinzipen
von [Prims Algorithmus](https://en.wikipedia.org/wiki/Prim%27s_algorithm), welcher von Robert Prim entwickelt wurde und
einem "Greedy"-Ansatz folgt. Zusammengefasst bedeutet dies, dass das Hauptziel ist den Spannbaum so minimal wie möglich
zu halten. Das Wort "Greedy", was zu Deutsch gierig bedeutet, kann wörtlich genommen werden, denn sobald es eine
Möglichkeit gibt, um den Baum zu erweitern wird diese wahrgenommen. In der ursprünglichen Definition werden die
verschiedenen Teile mit einem wertenden Zufallsprinzip ausgewählt. In der oben erklärten Implementierung wird die
Gewichtung zur Vereinfachung weggelassen.

<a name="nicht-zusammenhängend"></a>

### Nicht Zusammenhängend

<div align="center">
![nicht zusammenhängend](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/nicht_zusammenhängender_irrgarten.png)
<div style="font-style: italic">
    Die Abbildung oberhalb zeigt ein nicht zusammenhängender Irrgarten.
</div>
</div>
<br />

Im Rahmen dieser Projektarbeit wurde keine algorithmische Erstellungs entwickelt, um nicht zusammenhängende Irrgärten zu
erstellen. In diesem Abschnitt wird aber erklärt wie ein solcher Ansatz funktionieren würde und wie sich ein solcher vom obigen unterscheiden würde. Bei nicht zusammenhägenden Irrgärten spricht man auch oft von "nicht Perfekten" Irrgärten. Ein nicht perfekter Irrgarten entspricht nicht der eigentlichen Definition eines Irrgartens. Walter D. Pullen erklärt dies sehr detailliert in diesem bekannten [Text](http://www.astrolog.org/labyrnth/algrithm.htm). In seinem Text berschrebt und definiert er drei unterschiedliche Varianten von nicht perfekten Irrgärten:

* **Geflechtet**: Ohne Sackgassen (mit Kreisen)
* **Spärlich**: nicht alle Bereiche sind miteinander verbunden
* **Unikursal**: Ohne Kreuzungen (bzw. ohne Entscheidungen)

Offensichtlich können diese Typen auch miteinander vermischt werden, woraus wieder neue Zusammensetzungen entstünden. Im
Allgemeinen haben sie aber alle gemeinsam, dass sie je nach Umständen nicht von der Rechte-Hand Methode gelöst werden können. Dies passiert beispielsweise, wenn es einen Kreis im Irrgarten gibt, denn sobald der Algorithmus der Rechten Hand Regel dort ankommt, verfällt dieser in eine endlose Schleife von Umdrehungen (Die Abbildung oberhalb zeigt eine solche Situation). Genau deswegen braucht es dann den Pledge-Algorithmus, der sich auch durch nicht zusammenhängende Irrgärten manövrieren kann.

Die eigentliche Frage ist aber, wie die nicht perfekte Irrgärten erstellt werden. Einen nicht zusammenhängenden Irrgarten zufällig zu Erstellen ist grundsätzlich keine effiziente Idee. Ein von Grund auf nicht perfekter Irrgarten zu bauen ist sehr umständlich, da für jede erstellte Konstruktion (Anordnung von Wänden) kontrolliert werden müsste, ob diese Regel konform ist und ob es noch einen Weg durch den Irrgarten gibt. Es müsste fast auf vorgefertigten Teilen basieren, was aber dann wieder dem Zufallsprinzip widersprechen würde. Folglich ist ein solcher Ansatz nicht realistisch umsetzbar, da die Grundlage realistischerweise ein zusammenhängender Irrgarten sein müsste. Es könnte also ein zusammenhängender Irrgarten erstellt werden und dieser dann mit einem weiteren Algorithmus bearbeitet werden. Der erweiterte Algorithmus würde beispielsweise Wandteile entfernt, was dann einen  Kreis schaffen würden. 

Im Rahmen dieser Projektarbeit wurde auf ein solche Vorgehen verzichtet. Um diesen Effekt aber trotzdem demonstrieren zu können, gibt es in der Applikation links ein Menü mit verschiedenen vorgefertigten Karten, welche reingeladen werden können.

<a name="lösungsansätze"></a>

## Algorithmen für Wege aus den Irrgärten

So viele Irrgärten wie es gibt, so viele Möglichkeiten gibt es auch diese zu lösen. Dabei ist zu beachten, dass nicht
jeder Lösungsansatz für jeden Irrgarten funktioniert. Dazu kommt, dass je nach Irrgarten die Effektivität der
Algorithmen unterschiedlich sind. Die Unterscheidung findet dabei zwischen zusammenhängenden und nicht zusammenhängenden
Irrgärten statt. Auf zwei verschiedene Irrgärten, kommen zwei Algorithmen. Namentlich wären diese der Pledge-Algorithmus
und die Rechte-Hand Regel.

<a name="rechte-hand"></a>

### Rechte Hand Regel

<a name="erläuterung-2"></a>

#### Erläuterung

Die Rechte Hand Regel ist eine sehr simple Art, um aus einem Irrgarten zu entkommen. Zu beachten ist, dass es eine Regel
ist, um aus einem zusammenhängenden Irrgarten zu entkommen und nicht, um aus irgend einem Irrgarten zu entkommen. Ganz 
grundsätzlich ist die Idee dieser Regel, dass man immer der rechten Wand folgt, bis man am Ziel angekommen ist.

<a name="vor-und-nachteile-2"></a>

#### Vor -und Nachteile

Wie oben erwähnt ist ein grosser Nachteil der Regel, dass es ihm nicht möglich ist aus allen Irrgärten zu entkommen.
Diesen Nachteil macht er wieder gut, mit seiner effizienten und einfachen Art. Er geht kaum unnötig Wege, ist einfach zu
verstehen und leicht zu implementieren. Wenn man also einen Weg braucht, um aus zusammenhängenden Irgärten herauszukommen,
ist die Rechte Hand Regel genau richtig.

<a name="umsetzung-2"></a>

#### Umsetzung
Die Umsetzung der Rechten Hand Regel hält sich an 3 einfachen Schritten:

1. Wenn rechts frei ist, drehe dich nach rechts und gehe einen Schritt
2. Wenn rechts zu ist, aber vorne frei ist gehe nach vorne.
3. Wenn rechts zu ist und vorne zu ist, drehe nach links.

Man muss mit dem Versuch nach rechts zu gehen beginnen, weil man immer nach rechts gehen will, wenn man kann.
Mit diesen drei Schritten ist der Hauptteil der Implementierung  abgeschlossen. Nun muss man nur noch eine Schleife um
diese drei Schritte packen und fertig. Bei der unteren Implementierung gibt es noch einen "maxCounter". Dieser wird lediglich
gebraucht, für den Fall, das man sich nicht in einem Irrgarten befindet. Wäre dies der Fall, so würde das Programm endlos weiterlaufen,
da die Regel keine Lösung finden würde. Mit diesem Counter wird die maximale Anzahl der Durchläufe auf 25'000 gesetzt, was normalerweise
mehr als genug ist.




````javascript
let maxCounter = 25000; //Counter für nicht Irrgärten
while (!solved && maxCounter > 0) {
    
    //Schritt 1
    if (!isPlayerBlocked({...tmp_state, playerDir: tmp_state.playerDir + 90})) {
        turnRight();
        moveForward();
    //Schritt 2
    } else if (!isPlayerBlocked(tmp_state)) {
        moveForward();
    //Schritt 3    
    } else {
        turnLeft();
    }
    checkSolvedStatus();
    maxCounter--;
}
````

<a name="pledge"></a>

### Pledge-Algorithmus

<a name="erläuterung-1"></a>

#### Erläuterung

Der Pledge-Algorithmus ist einer der bekanntesten, um einen Irrgarten zu lösen. Dies kommt davon, dass er jeden
beliebigen Irrgarten lösen kann. Sei es Zusammenhängen, nicht Zusammenhängen oder eine Mischform. Der Pledge findet
immer einen Ausweg, sofern es einen gibt. Er findet dabei allerdings nicht immer den optimalen Weg, sondern nur eine von vielen möglichen Lösungen.
Im Grunde handelt es sich beim Pledge um eine Erweiterung der "Rechten Hand Regel" (wird weiter unten beschrieben). Im Gegensatz zu ihr merkt er sich aber seine Drehungen und weiss immer in welche Richtung er gerade schaut im Verhältnis zur Startposition. Dies passiert im
Allgemeinen, indem man eine Zahl Hinauf oder Herab zählt. Sobald man sich also um 90 Grad gedreht hat, wird die Zahl je
nach Richtung verändert. Zum einen hilft dies bei der Entscheidung an jeder Kreuzung, indem man sich nur nach Links
dreht, wenn die Zahl negativ ist. Zum anderen kann er mit dieser Eigenschaft die Probleme der Rechten-Hand Regel
überwinden. Sprich sich aus einem Nicht zusammenhängendem Stück zu befreien.

<a name="vor-und-nachteile-1"></a>

#### Vor -und Nachteile

Wie oben erwähnt ist es dem Pledge-Algorithmus möglich sich aus jedem Irrgarten zu befreien. Offensichtlich ist dies sein grösster
Vorteil. Des Weiteren ist seine Implementierung strikt und klar, dazu aber später noch mehr. Mit der Möglichkeit aus
jedem Irrgarten einen Ausweg zu finden, kommen auch ein Nachteil. Namentlich wird der Algorithmus einer der
Ineffizienteren sein, da er auch unrealistische Möglichkeit überprüft. Daraus schliesst sich, dass der Algorithmus an gewissen
Stellen unnötige Schritte erledigt.

Dazu kommt, dass es dem
Pledge-Algorithmus [nicht immer möglich ist einen Punkt im Irrgarten zu finden](https://de.wikipedia.org/wiki/Lösungsalgorithmen_für_Irrgärten#Pledge-Algorithmus)
. In anderen Worten ist der Algorithmus überfordert, wenn man ihn in einen Irrgarten schickt, um einen gewissen Punkt zu
finden.

<a name="umsetzung-1"></a>

#### Umsetzung

***Idee***<br>
![Error](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/pledge-idee.png)

Die Umsetzung basiert sich auf der obigen Abbildung.
<br>Als Erstes wird ein Richtungs-Counter gesetzt. Dieser wird gebraucht, um die Umdrehungen im Auge zu behalten.
<br> <br>Danach kommt schon die erste Schleife. Ihr Zweck ist es, dass der Algorithmus so lange läuft, bis der Ausweg
gefunden worden ist.
<br> <br>Folgend kommt eine weiter Schleife. In Ihr läuft der Player einfach bis er auf eine Wand trifft. Zusätzlich
wird immer noch geschaut, ob man nach links abbiegen könnte. Für das muss es zum einen Links frei sein und zum anderen
darf der Counter nicht grösser oder gleich 0 sein. Sollte beide Bedingungen erfüllt sein dreht man nach Links und der
Counter wird Hochgezählt. Nach dieser Abfrage beginnt die Schleife von vorne. Die kleine Abfrage dazwischen ist
notwendig, um eine Möglichkeit sich links zu drehen, zu können nicht zu verpassen.
<br><br> Diese Schleife ist gefolgt, von einigen IF Abfragen. Man weiss ja nun, dass man sich an einer Wand befindet und
diese Abfragen stehen für die verschiedenen Fälle, in denen man sich befinden kann:


1. Die erste Abfrage steht für den Fall, dass Rechts frei ist links aber nicht:
   <br>
   ![Error](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/fall-1.png)
   <br>
   Hier wird nach rechts gedreht und der Counter herab gezählt.


2. Diese Abfrage steht für den Fall das rechts zu ist links aber frei:
   <br>
   ![Error](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/all-2.png)
   <br>
   Hier wird nach links gedreht und der Counter hoch gezählt, sofern der Counter Negativ ist.


3. Danach wird überprüft, ob Links und Rechts frei ist:
   <br>
   ![Error](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/fall-3.png)
   <br>
   Ist dies der Fall, wird der Counter abgefragt und wenn dieser negativ ist, drehen wir uns nach links und wenn er 0 ist
   nach rechts.


4. Wenn nichts von vorher zutrifft, dann muss es die Situation sein, wo man von beiden Seiten geblockt ist.
   <br>
   ![Error](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/fall-4.png)
   <br>
   In diesem Fall dreht sich der Player einfach zweimal nach rechts und der Counter wird auch zweimal nach Unten
   gezählt.

<br>Zum Schluss wird überprüft, ob wir fertig sind. Wenn ja, wird der Boolean "solved" auf true gesetzt ansonsten nicht.
Nach dieser Überprüfung beginnt es wieder oben in der While-Schleife.


```javascript
let turnCounter = 0; //Counter für die Umdrehungen

while (!solved) { //While-Schleife die Solange geht, bis man fertig ist.

    while (!isPlayerBlocked(tmp_state)) { //Solange vorne Frei ist.
        moveForward(); //mach einen Schritt
        // wenn links frei ist und turnCounter kleiner als 0, drehe nach links
        if (!isPlayerBlocked({...tmp_state, playerDir: tmp_state.playerDir - 90}) && turnCounter < 0) {
            turnLeft();
            turnCounter++;
        }
    }

    //Fall 1
    if (isPlayerBlocked({...tmp_state, playerDir: tmp_state.playerDir - 90}) && !isPlayerBlocked({
        ...tmp_state,
        playerDir: tmp_state.playerDir + 90
    })) {
        turnRight();
        turnCounter--;

        //Fall 2
    } else if (!isPlayerBlocked({...tmp_state, playerDir: tmp_state.playerDir - 90}) && isPlayerBlocked({
        ...tmp_state,
        playerDir: tmp_state.playerDir + 90
    }) && turnCounter < 0) {
        turnLeft();
        turnCounter++;

        //Fall 3
    } else if (!isPlayerBlocked({...tmp_state, playerDir: tmp_state.playerDir - 90}) && !isPlayerBlocked({
        ...tmp_state,
        playerDir: tmp_state.playerDir + 90
    })) {
        if (turnCounter < 0) {
            turnLeft();
            turnCounter++;

        } else {
            turnRight();
            turnCounter--;
        }

        //Fall 4
    } else {
        turnRight();
        turnCounter--;
        turnRight();
        turnCounter--;
    }

    //Überprüfung, ob man beim Ausgang steht.
    checkSolvedStatus();
}
```


<a name="visualisierung"></a>

## Visualisierung

Das GUI der Applikation ist mit React.js und Electron.js gebaut. React ist ein Webframework, welches sich für
Clientseitiges ausführen von HTML/CSS und JavaScript eignet. Da die App aber als Desktop-Applikation läuft, wird zudem
Electron.js verwendet, welches als eine Art simulierter Browser die Webtechnologien auf den Desktop bringen. Microsoft
Teams oder Spotify funktionieren beispielsweise auch so.

Mit HTML/CSS ist es nun möglich das GUI wie eine Webseite aufzubauen und Veränderungen live zu rendern. Die zu lösende
Schwierigkeit liegt deshalb die effektive Visualisierung des Irrgartens. Bei der Erstellung entsteht durch den
Algorithmus ein 2D Array, welches mit 1 und 0 festhält, ob es eine Wand hat oder nicht. Es wäre nun möglich den
Irrgarten mit einem HTML-Canvas oder beispielsweise einem HTML-Div umzusetzen. Der Nachteil von einem solchen Ansatz
wäre, dass die Teile sich nicht proportional zueinander verhalten würden. So wäre das Stylen und besonders das
verwaltern und verändern eines solchen Ansatzes sehr fehleranfällig und unzuverlässig. Ein viel angenehmerer Ansatz ist
es eine HTML-Tabelle zu generieren. Das 2D Array wird dann direkt in eine Tabelle mit quadratischen Feldern
uminterpretiert. Die visuellen Unterschiede zwischen der Wand und dem Weg werden mit reinem CSS erzeugt. Der grösste
Vorteil der Tabelle liegt aber darin, dass jedes Feld von jedem anderen abhängig ist und so praktisch keine unerwartet
Darstellungen zustande kommen können. Indem weitere Schritte
zum [Conditional Rendering](https://reactjs.org/docs/conditional-rendering.html) hinzugefügt werden, können so auch die
Start- und Endposition, sowie die Spielfigur platziert werden.
<a name="Endresultat"></a>

## Endresultat

![Application](https://gitlab.com/LewinGerber/pledge-labyrinth/-/raw/master/Ressources/application_screenshot.png)

Die entstandene Applikation vereint die zweiteilige Problemstellung in eine einzige zusammenpassende Lösung. Auf teils spielerische Weise wird aufgezeigt wie die Erstellung von Irrgärten in der Praxis aussieht und wie verschiedene Irrgärten von den beiden Lösungsansätzen jeweils gelöst werden. Grundlage für die Applikation ist React.js in Verbindung mit Electron.js. Mit diesen beiden Technologien wurde die Logik basieren auf JavaScript umgesetzt und dann mit HTML und CSS visualisiert. Die Applikation kann auf allen Plattformen ausgeführt werden.

<a name="funktionsweise"></a>

### Funktionsweise

Sobald die Applikation gestartet wird, ist die grafische Oberfläche zu sehen, welche essenziell so aufgebaut ist wie eine Webseite. Die grosse Visualisierungs-Fläche ist unterhalb und auf der rechten Seite durch zwei Menüs eingerahmt. Auf der rechten Seite können Einstellungen zur Erstellung oder dem Einlesen von vorgefertigten Irrgärten getätigt werden. Im Menü unterhalb kann bestimmt werden, welcher Algorithmus zum Einsatz kommt und zudem kann der Irrgarten und dessen Visualisierung manuell angepasst werden.

Wenn ein neuer zusammenhängender Irrgarten erstellt werden soll, kann im Menü rechts "generieren" betätigt werden, was ein neuer Irrgarten erstellt. Im Menü rechts sind unter "maps" verschiedene vorgefertigte, nicht perfekte Irrgärten schon rein programmiert. Falls eine komplett eigener Irrgarten gebaut werden soll, kann im Menü unter der Visualisierungsebene mit dem Stift-Werkzeug der Irrgarten direkt bearbeitet werden. Sobald der Bearbeitungsmodus aktiviert wurde (angezeigt durch die blau blinkende Schaltfläche beim Stift-Icon) kann mit Links-Klick ein Block hinzugefügt werden und mit Rechts-Klick ein Block entfernt werden.

Aus technischer Sicht funktioniert die Applikation so, dass alle Elemente vom Irrgarten über einen "Reducer" der ganzen Applikation zur Verfügung gestellt werden. Die widerspricht der Ursprungslogik hinter dem React-Entwicklungsprozess, welche grundsätzlich auf dem [Parsen](https://www.duden.de/rechtschreibung/parsen) von Properties aufbaut. Der Vorteil von einem solchen Ansatz ist, allerdings dass Funktionen von Komponenten auf verschiedensten Hierarchiestufen ohne Zwischenschritte aufgerufen werden können. Nur so ist es möglich eine 100 % clientseitge React-Applikation übersichtlich und performant zu halten.

<a name="gamification"></a>

#### Gamification

Da diese Problemstellungen von der Visualisierung lebt, wurden für diese Applikation weitere visuelle Features
hinzugefügt. Das Endresultat ist nun nicht nur eine Applikation zur Visualisierung, sondern auch ein Erlebnis für den Benutzer. 
Dieser Prozess wird hier als Gamification bezeichnet, da es vor allem darum ging das Lösen des Labyrinths
interessanter zu gestalten. So gibt es im Menü links unterhalb von den Erstellungsoptionen die Möglichkeit den Charakter
zu wechseln. Man kann zwischen verschiedenen Bildern auswählen und sobald eines festgelegt wurde, erscheint dieses als
Figur auf der Visualisierungs-Fläche. Mit dem Bearbeitungsmodus ist es möglich, dass man jeden beliebigen Irrgarten bauen kann, was ermöglicht, dass der Algorithmus interessant getestet werden kann.

<a name="grenzen"></a>

### Grenzen und Probleme

Die Grenze dieser Applikation liegt darin, dass nur zusammenhängenden Irrgärten erstellt werden können. Dieser Grenze
wird aktiv entgegengewirkt, indem vorgefertigte nicht zusammenhängende Irrgärten geladen werden können und indem dem
Benutzer die Möglichkeit gelassen wird die erstellten Irrgärten anzupassen. Wie im Kapitel über die Erstellung von nicht zusammenhängenden Irrgärten erklärt wird ist das Erstellen nur eine Frage des entwickeln eines weiteren Algorithmus, welcher die bestehenden zusammenhängenden Irrgärten strategisch anpasst.

<a name="reflexion"></a>

## Reflexion

Das Endresultat dieser Projektarbeit ist mehr als nur zufriedenstellend. Weil wir eine nahe zu perfekte Arbeitsteilung
hatten, konnten wir den beiden Problemstellungen komplett auf den grundgehen und haben unsere Erkenntnisse ansprechend
in eine Applikation inegriert. Zudem haben wir unsere Applikation nicht nur zu einer Demonstration der Probleme gemacht,
sondern ein Erlebnis rundherum gebaut. Wir sind alle sehr zufrieden wie interessant es ist mit der Applikation
herumzuspielen und Dinge auszuprobieren. Gelernt haben wir von dieser Projektarbeit vor allem wie Irrgärten
funktionieren, was deren Eigenschaften sind und wie diese angewendet werden können. Am meisten Mühe hatten wir allerdings mit der Dokumentation. Da besonders die beiden Lösungsalgorithmen nicht so viel hergeben, war es schwer eine Dokumentation darum zu bauen, da es nicht wirklich viel interessatnes dazu zu sagen gab.
