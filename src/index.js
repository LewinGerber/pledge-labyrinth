import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';

import MazeProvider from './context/MazeProvider';
import MenuProvider from './context/MenuProvider';

import './index-style.css';

ReactDOM.render(
    <React.StrictMode>
        <MenuProvider>
            <MazeProvider>
                <App />
            </MazeProvider>
        </MenuProvider>
    </React.StrictMode>,
    document.getElementById('root')
);
