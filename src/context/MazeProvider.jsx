import React, { useReducer, createContext } from 'react';

import mazeReducer from './maze-reducer';

export const mazeContext = createContext();

const MazeProvider = ({ children }) => {
    /* 
        useReducer containing global state with:
        - maze                    2-dimensional array containing the generated maze [[1, 0, 1, 0, 0]]
        - solvingSteps            2-dimensional array containing all steps nessecary for solving the maze [[X, Y, DIR]]         
        - playerPos, playerDir     current position [X, Y] and direction of player in degrees (0, 90, 180, 270)
        - startPos, endPos        start-/end positions [X, Y]
        - agentIndex              index of the file for the current agent image (/images/characters/)  
    */
    const [state, dispatch] = useReducer(mazeReducer, {
        maze: [[]],
        solvingSteps: [[]],
        playerPos: [0, 0],
        playerDir: 90,
        startPos: [0, 0],
        endPos: [0, 0],
        agentIndex: 0,
    });

    /* PRE-GENERATED MAPS */

    const maps = [
        // MAP 0
        [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
        // MAP 1
        [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1],
            [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
            [1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
        // MAP 2
        [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ],
    ];

    /* DISPATCH EVENTS */

    // dispatch event to generate a random maze
    const generateMaze = (dimensions) => {
        dispatch({
            type: 'GENERATE_MAZE',
            payload: dimensions,
        });
    };

    // dispatch event to move the player foward within the maze (if possible)
    const movePlayerForward = () => {
        dispatch({
            type: 'MOVE_PLAYER_FORWARD',
        });
    };

    // dispatch event to turn the player into the given direction ('LEFT', 'RIGHT')
    const turnPlayer = (dir) => {
        dispatch({
            type: 'TURN_PLAYER',
            payload: dir,
        });
    };

    // dispatch event to set the player position directly (for animation)
    const setPlayerPosition = (newPos) => {
        dispatch({
            type: 'SET_PLAYER_POS',
            payload: newPos,
        });
    }

    // dispatch event to set the new index for the character image
    const setAgentIndex = (newIndex) => {
        dispatch({
            type: 'SET_AGENT_INDEX',
            payload: newIndex,
        });
    } 

    // dispatch event to solve the maze and set the solvingSteps state
    const solveMaze = (algorithm) => {
        dispatch({
            type: 'SOLVE_MAZE',
            payload: algorithm,
        });
    };

    // dispatch event to set the maze
    const setMaze = (newMaze) => {
        dispatch({
            type: 'SET_MAZE',
            payload: newMaze,
        });
    };


    // dispatch event to load pre generated maze maps into the state (with integer of map as parameter)
    const loadMap = (mapIndex) => {
        dispatch({
            type: 'LOAD_MAP',
            payload: maps[mapIndex],
        });
    };

    // resets the solvingSteps array
    const resetSolvingSteps = () => {
        dispatch({
            type: 'RESET_SOLVING_STEPS'
        });
    }

    return (
        <mazeContext.Provider
            value={{
                mazeState: state,
                generateMaze: generateMaze,
                solveMaze: solveMaze,
                setMaze: setMaze,
                movePlayerForward: movePlayerForward,
                setPlayerPosition: setPlayerPosition,
                setAgentIndex: setAgentIndex,
                turnPlayer: turnPlayer,
                loadMap: loadMap,
                resetSolvingSteps: resetSolvingSteps,
            }}
        >
            {children}
        </mazeContext.Provider>
    );
};

export default MazeProvider;
