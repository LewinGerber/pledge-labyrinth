/* REDUCER FUNCTIONS */

/* REDUCER */

const menuReducer = (state, action) => {
    switch (action.type) {
        case 'TOGGLE_IN_PROGRESS':
            return {
                ...state,
                inProgress: !state.inProgress,
            }

        case 'SET_ANIMATION_SPEED':
            return {
                ...state,
                animationSpeed: action.payload,
            }

        case 'SWITCH_ALGORITHM':
            return {
                ...state,
                algorithm: action.payload,
            }

        case 'TOGGLE_EDITING_MODE':
            return {
                ...state,
                editingMode: !action.payload,
            }
    

        default:
            return state;
    }
};

export default menuReducer;
