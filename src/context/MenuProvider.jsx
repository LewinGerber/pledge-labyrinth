import React, { useReducer, createContext } from 'react';

import menuReducer from './menu-reducer';

export const menuContext = createContext();

const MenuProvider = ({ children }) => {
    /* 
        useReducer containing global state with:
        - inProgress      true while the animation is ongoing
        - animationSpeed  speed of the player animation (0-100)
        - algorithm       current selected algorithm (RIGHT_HAND, PLEDGE)
        - editingMode     boolean to indicate if the user is editing the maze
    */
    const [state, dispatch] = useReducer(menuReducer, {
        inProgress: false,
        animationSpeed: 70,
        algorithm: 'RIGHT_HAND',
        editingMode: false,
    });

    /* DISPATCH EVENTS */ 

    // dispatch event to toggle the inProgress state
    const toggleInProgress = () => {
        dispatch({
            type: 'TOGGLE_IN_PROGRESS',
        });
    };

    // dispatch event to set the animationSpeed
    const setAnimationSpeed = (speed) => {
        dispatch({
            type: 'SET_ANIMATION_SPEED',
            payload: speed,
        });
    };

    // dispatch event to switch between the different algorithms
    const setAlgorithm = (algorithm) => {
        dispatch({
            type: 'SWITCH_ALGORITHM',
            payload: algorithm,
        });
    };

    // dispatch event to toggle editing mode
    const toggleEditingMode = () => {
        dispatch({
            type: 'TOGGLE_EDITING_MODE',
            payload: state.editingMode,
        });
    }

    return (
        <menuContext.Provider
            value={{
                menuState: state,
                toggleInProgress: toggleInProgress,
                setAnimationSpeed: setAnimationSpeed,
                setAlgorithm: setAlgorithm,
                toggleEditingMode: toggleEditingMode,
            }}
        >
            {children}
        </menuContext.Provider>
    );
};

export default MenuProvider;
