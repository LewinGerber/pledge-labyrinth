/* REDUCER FUNCTIONS */

// generates a random maze with given width and height and sets the maze state correspondingly
const generateMaze = (options) => {

    let { width, height, empty: EMPTY = 0, wall: WALL = 1, } = options;

    if (width % 2 === 0) {
        width--;
    }

    if (height % 2 === 0) {
        height--;
    }

    const OOB = {};

    if (!width || !height) {
        throw new Error('Missing required `width` & `height` options!');
    }

    const out = new Array(height);
    for (let y = 0; y < out.length; y++) {
        out[y] = new Array(width).fill(WALL);
    }

    const lookup = (field, x, y, defaultValue = EMPTY) => {
        if (x < 0 || y < 0 || x >= width || y >= height) {
            return defaultValue;
        }

        return field[y][x];
    }

    const walls = [];
    const makePassage = (x, y) => {
        out[y][x] = EMPTY;

        const candidates = [
            { x: x - 1, y },
            { x: x + 1, y },
            { x, y: y - 1 },
            { x, y: y + 1 },
        ];

        candidates.forEach((wall) => {
            if (lookup(out, wall.x, wall.y) === WALL) {
                walls.push(wall);
            }
        });
    }

    // pick random point and make it a passage
    makePassage(Math.random() * width | 0, Math.random() * height | 0);

    while (walls.length !== 0) {
        const { x, y } = walls.splice((Math.random() * walls.length) | 0, 1)[0];

        const left = lookup(out, x - 1, y, OOB);
        const right = lookup(out, x + 1, y, OOB);
        const top = lookup(out, x, y - 1, OOB);
        const bottom = lookup(out, x, y + 1, OOB);

        if (left === EMPTY && right === WALL) {
            out[y][x] = EMPTY;
            makePassage(x + 1, y);
        } else if (right === EMPTY && left === WALL) {
            out[y][x] = EMPTY;
            makePassage(x - 1, y);
        } else if (top === EMPTY && bottom === WALL) {
            out[y][x] = EMPTY;
            makePassage(x, y + 1);
        } else if (bottom === EMPTY && top === WALL) {
            out[y][x] = EMPTY;
            makePassage(x, y - 1);
        }
    }

    // checks if the maze has wall all around it
    let isGenerationFinished = true;
    out.forEach((row, rowIndex) => {
        row.forEach((column, columnIndex) => {
            if ((rowIndex === 0 || rowIndex === out.length - 1 || columnIndex === 0 || columnIndex === row.length - 1) && column === 0) {
                isGenerationFinished = false;
            }
        });
    });

    if (isGenerationFinished) {
        console.log(out);
        return out;
    } else {
        return generateMaze(options);
    }
};

// moves the player forward corresponding to its playerDir and returns the new playerPos
const movePlayerForward = (state) => {
    let tmp_playerPos = [...state.playerPos];

    if (!isPlayerBlocked(state)) {
        switch (state.playerDir) {
            case 270:
                tmp_playerPos[0] -= 1;
                break;

            case 90:
                tmp_playerPos[0] += 1;
                break;

            case 180:
                tmp_playerPos[1] -= 1;
                break;

            case 0:
                tmp_playerPos[1] += 1;
                break;

            default:
                break;
        }
    }

    return tmp_playerPos;
};

// turns the player left and returns the new playerDir in degrees
const turnPlayer = (playerDir, dir) => {
    let tmp_playerDir = playerDir;

    switch (dir) {
        case 'LEFT':
            tmp_playerDir -= 90;
            if (tmp_playerDir < 0) {
                tmp_playerDir = 270;
            }
            break;

        case 'RIGHT':
            tmp_playerDir += 90;
            if (tmp_playerDir === 360) {
                tmp_playerDir = 0;
            }
            break;

        default:
            break;
    }

    return tmp_playerDir;
};

// returns boolean (true) if there is a wall in front of the player
const isPlayerBlocked = ({ maze, playerPos, playerDir }) => {
    if (maze[playerPos[0] - 1][playerPos[1]] === 1 && (playerDir === 270 || playerDir === -90)) {
        // if there is a wall above the player (X - 1)
        return true;

    } else if (maze[playerPos[0] + 1][playerPos[1]] === 1 && playerDir === 90) {
        // if there is a wall below the player (X + 1)
        return true;

    } else if (maze[playerPos[0]][playerPos[1] - 1] === 1 && playerDir === 180) {
        // if there is a wall to the left of the player (Y - 1)
        return true;

    } else if (maze[playerPos[0]][playerPos[1] + 1] === 1 && (playerDir === 0 || playerDir === 360)) {
        // if there is a wall to the right of the player (Y + 1)
        return true;
    }

    // return false if there is no wall
    return false;
};

// solves the maze and returns an array containing all steps for solving it
const solveMaze = (state, algorithm) => {
    let solved = false;

    let tmp_state = {
        ...state,
        solvingSteps: [[...state.playerPos, state.playerDir]],
    };

    const turnLeft = () => {
        tmp_state.playerDir = turnPlayer(tmp_state.playerDir, 'LEFT');
        tmp_state.solvingSteps.push([...tmp_state.playerPos, tmp_state.playerDir]);
    };

    const turnRight = () => {
        tmp_state.playerDir = turnPlayer(tmp_state.playerDir, 'RIGHT');
        tmp_state.solvingSteps.push([...tmp_state.playerPos, tmp_state.playerDir]);
    };

    const moveForward = () => {
        tmp_state.playerPos = movePlayerForward(tmp_state);
        tmp_state.solvingSteps.push([...tmp_state.playerPos, tmp_state.playerDir]);
    };

    const checkSolvedStatus = () => {
        if (tmp_state.playerPos[0] === state.endPos[0] && tmp_state.playerPos[1] === state.endPos[1]) {
            solved = true;
        }
    };

    switch (algorithm) {
        case 'RIGHT_HAND':
            let maxCounter = 25000;
            while (!solved && maxCounter > 0) {
                if (!isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir + 90 })) {
                    // if the right side of the player is free, turn right, save and move forward
                    turnRight();
                    moveForward();
                } else if (!isPlayerBlocked(tmp_state)) {
                    // if there is no wall in front of the player, go forward
                    moveForward();
                } else {
                    // if the player is blocked (forward and right side), turn the player left
                    turnLeft();
                }

                // set solved to true if player is on endPos and decrease maxCounter
                checkSolvedStatus();
                maxCounter--;
            }

            break;

        case 'PLEDGE':
            let turnCounter = 0;
            while (!solved) {
                // while the player is free, move player forward 
                while (!isPlayerBlocked(tmp_state)) {
                    moveForward();

                    // wenn links frei ist und turnCounter kleiner als 0, drehe nach links
                    if (!isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir - 90 }) && turnCounter < 0) {
                        turnLeft();
                        turnCounter++;
                    }
                }

                if (isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir - 90 }) && !isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir + 90 })) {
                    turnRight();
                    turnCounter--;

                } else if (!isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir - 90 }) && isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir + 90 }) && turnCounter < 0) {
                    turnLeft();
                    turnCounter++;

                } else if (!isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir - 90 }) && !isPlayerBlocked({ ...tmp_state, playerDir: tmp_state.playerDir + 90 })) {
                    if (turnCounter < 0) {
                        turnLeft();
                        turnCounter++;

                    } else {
                        turnRight();
                        turnCounter--;
                    }

                } else {
                    turnRight();
                    turnCounter--;
                    turnRight();
                    turnCounter--;
                }

                // set solved to true if player is on endPos
                checkSolvedStatus();
            }

            break;

        default:
            break;
    }

    return tmp_state.solvingSteps;
};

/* REDUCER */

const mazeReducer = (state, action) => {
    switch (action.type) {
        case 'GENERATE_MAZE':
            return {
                ...state,
                playerPos: [1, 1],
                playerDir: 90,
                startPos: [1, 1],
                endPos: [action.payload.height % 2 === 0 ? action.payload.height - 3 : action.payload.height - 2, action.payload.width % 2 === 0 ? action.payload.width - 3 : action.payload.width - 2],
                maze: generateMaze(action.payload)
            };

        case 'MOVE_PLAYER_FORWARD':
            return {
                ...state,
                playerPos: movePlayerForward(state),
            };

        case 'TURN_PLAYER':
            return {
                ...state,
                playerDir: turnPlayer(state.playerDir, action.payload),
            };

        case 'SET_PLAYER_POS':
            return {
                ...state,
                playerPos: [action.payload[0], action.payload[1]],
                playerDir: action.payload[2],
            };

        case 'SET_AGENT_INDEX':
            return {
                ...state,
                agentIndex: action.payload,
            };

        case 'SOLVE_MAZE':
            return {
                ...state,
                solvingSteps: solveMaze(state, action.payload),
            };

        case 'SET_MAZE':
            return {
                ...state,
                maze: action.payload,
            };

        case 'LOAD_MAP':
            return {
                ...state,
                playerPos: [1, 1],
                playerDir: 90,
                startPos: [1, 1],
                endPos: [action.payload.length - 2, action.payload[1].length - 2],
                maze: action.payload,
            };

        case 'RESET_SOLVING_STEPS':
            return {
                ...state,
                solvingSteps: [[]],
            }

        default:
            return state;
    }
};

export default mazeReducer;
