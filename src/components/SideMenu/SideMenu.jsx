import React, { useContext, useState, useEffect } from 'react';

import { Button, Fab, Accordion, AccordionSummary, AccordionDetails, Grow, Dialog } from '@material-ui/core';
import { mazeContext } from '../../context/MazeProvider';
import { menuContext } from '../../context/MenuProvider';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SlowMotionVideoIcon from '@material-ui/icons/SlowMotionVideo';
import GradientIcon from '@material-ui/icons/Gradient';
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ForwardIcon from '@material-ui/icons/Forward';

import './side-menu-style.css';

import { windowOpenNoOpener } from 'custom-electron-titlebar/lib/common/dom';

const SideMenu = (props) => {

    const { mazeState, generateMaze, loadMap } = useContext(mazeContext);
    const { menuState } = useContext(menuContext);

    const [agentPopUp, setAgentPopUp] = useState(false);
    const [ windowDimensions, setWindowDimensions] = useState({width: 0, height: 0});
    
    // calculates the maze height and width from the window size
    function calculateMazeSize() {
        let mazeRow;
        let mazeColumn = Math.floor(window.innerHeight / 34.5);
        if (window.innerWidth < 1250) {
            mazeRow = Math.floor(window.innerWidth / 38);
        } else {
            mazeRow = Math.floor(window.innerWidth / 35.5);
        }
        return {width: mazeRow, height: mazeColumn}
    }

    //returns the new maze value if the resize event is successful
    // todo: not always working flawlessly
    const resizeMazeEvent = () => {
        if( (window.innerWidth && window.innerHeight)
            && (window.innerHeight !== '' && windowOpenNoOpener.innnerWidth !== '')
            && (windowDimensions.width !== window.innerWidth || windowDimensions.height !== window.innerHeight)) {
            setWindowDimensions({width: window.innerWidth, height: window.innerHeight});
            if(mazeState.maze[0][0] !== undefined) 
                generateMaze(calculateMazeSize());
        }
    }

    useEffect(() => {
        let timeoutId = null;
        const resizeListener = () => {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(() => resizeMazeEvent(), 200);
        }
        window.addEventListener('resize', resizeListener);
        return(() => window.removeEventListener('resize', resizeListener));
    }, [windowDimensions]);

    // useEffect on load
    useEffect(() => {
        generateMaze(calculateMazeSize());
    }, []);

    const Map = (props) => (
        <div id="sm-map-card">
            <div id="sm-card-content">
                <h2 style={{zIndex: 2, fontWeight: 'bold'}}>{props.title}</h2>
                <div id="sm-maze-bg"/>
                <Button disabled={menuState.inProgress} variant="contained" id="sm-map-set-btn" onClick={() => loadMap(props.mapId)}>
                    <SlowMotionVideoIcon style={{ marginRight: '0.5rem', marginLeft: '0.25rem' }} /> SET
                </Button>
                <div id="sm-cover-shape" />
            </div>
        </div>
    );

    const AgentSelection = (props) => {
        const { mazeState, setAgentIndex } = useContext(mazeContext);
        const [agentIndex, setCurrentIndex] = useState(mazeState.agentIndex);

        const incrementCurrentIndex = () => 
            (agentIndex < images.length-1) ? setCurrentIndex(agentIndex + 1): null;
        
        const decrementCurrentIndex = () => 
            (agentIndex > 0) ? setCurrentIndex(agentIndex - 1) : null;
        
        function importImagesFromDir(r) {
            let images = [];
            r.keys().map((item, index) => 
                images.push((images[item.replace('./', '')] = r(item)).default));
            return images;
        } 
        const images = importImagesFromDir(require.context('./../../images/characters', false, /\.(png|jpe?g|svg)$/));

        //helper function for replacing all occurences of a string
        function replaceAll(string, search, replace) {
            return string.split(search).join(replace);
        }

        return (
            <Dialog open={agentPopUp} onClose={() => setAgentPopUp(false)} id="sm-agent-dialog">
                <div id="sm-agent-popup">
                    <h1>Charakter auswählen:</h1>
                    <div id="sm-agent-slider">
                        <Fab
                            id="trans-fab-btn" 
                            className="sm-left-fab"
                            onClick={decrementCurrentIndex}
                            style={agentIndex === 0 ? {display: 'none'}: null} 
                        >
                            <ChevronLeftIcon />
                        </Fab>
                        <div id="sm-agent-area">
                            <div id="sm-agent-preview">
                                <img src={images[agentIndex]} alt="character-preview"/>
                            </div>
                        </div>
                        <Fab 
                            id="trans-fab-btn" 
                            className="sm-right-fab"
                            style={agentIndex === images.length-1 ? {display: 'none'}: null}
                            onClick={incrementCurrentIndex} 
                        >
                            <ChevronRightIcon />
                        </Fab>
                        <h2>
                            {
                                images[agentIndex] !== undefined 
                                ? replaceAll(
                                    images[agentIndex].substr(14).split('.')[0],
                                    '_',
                                    " "
                                )
                                : null
                            }
                        </h2>
                    </div>
                    <Button onClick={() => {setAgentIndex(agentIndex); setAgentPopUp(false)}} variant="contained" id="sm-agent-select-btn" style={{ backgroundColor: 'blue !important' }}>
                        Auswählen <ForwardIcon style={{marginLeft: '0.5rem'}}/>
                    </Button>
                </div>
            </Dialog>
        );
    };

    return (
        <div id="sm-container">
            <AgentSelection />
            <div id="sm-title">
                <h2>Irrgarten</h2>
            </div>
            <div id="sm-map-generator">
                <Accordion id="sm-accordion-transparent" TransitionProps={{ unmountOnExit: true }}>
                    <AccordionSummary
                        id="sm-accordion-underline"
                        expandIcon={<ExpandMoreIcon style={{ color: 'white' }} />}
                        aria-controls="panel2a-content"
                    >
                        Erstellung
                </AccordionSummary>
                    <AccordionDetails id="sm-accordion-details">
                        <Button disabled={menuState.inProgress} variant="contained" id="sm-generate-btn" onClick={() => generateMaze(calculateMazeSize())}>
                            <GradientIcon style={{ paddingRight: '0.5rem' }} /> erstellen
                        </Button>
                        <p style={{fontStyle: 'italic', marginTop: '0.5rem', fontSize: '0.75rem', color: 'grey'}}>zusammenhängender Irrgarten erstellen</p>
                    </AccordionDetails>
                </Accordion>
            </div>
            <div id="sm-maps">
                <Accordion
                    id="sm-accordion-transparent"
                    TransitionProps={{ unmountOnExit: true }}
                    TransitionComponent={Grow}
                >
                    <AccordionSummary
                        id="sm-accordion-underline"
                        expandIcon={<ExpandMoreIcon style={{ color: 'white' }} />}
                        aria-controls="panel2a-content"
                    >
                        Karten
                </AccordionSummary>
                    <AccordionDetails id="sm-accordion-details">
                    <p style={{fontStyle: 'italic', marginTop: '0.5rem', fontSize: '0.75rem', color: 'grey'}}>nicht perfekte Irrgärten laden</p>
                        <Map title="Geflechtet" mapId="0"/>
                        <Map title="Spärlich" mapId="1" />
                        <Map title="Unikursal" mapId="2" />
                    </AccordionDetails>
                </Accordion>
            </div>
            <Button disabled={menuState.inProgress} variant="contained" id="sm-menu-btn" style={{width: '235px', marginTop: '1rem'}} onClick={() => setAgentPopUp(!agentPopUp)}>
                <DirectionsWalkIcon style={{ marginRight: '0.5rem', marginLeft: '0.25rem' }} /> Charakter
            </Button>
        </div>
    );
}

export default SideMenu;
