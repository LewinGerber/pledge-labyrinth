import React, { useContext, useEffect } from 'react';

import { Button, Slider, Tooltip, Zoom, withStyles, Fab, CircularProgress } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import CreateIcon from '@material-ui/icons/Create';
import PanToolIcon from '@material-ui/icons/PanTool';
import MazeIcon from '../icons/MazeIcon';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import RotateRightIcon from '@material-ui/icons/RotateRight';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import BorderClearIcon from '@material-ui/icons/BorderClear';

import { mazeContext } from '../../context/MazeProvider';
import { menuContext } from '../../context/MenuProvider';

import './labyrinth-menu-style.css';

const CustomSlider = withStyles({
    thumb: {
        '&:focus, &:hover': {
            boxShadow: 'none',
        },
    },
})(Slider);

const LabyrinthMenu = () => {

    const { mazeState, solveMaze, movePlayerForward, turnPlayer, setPlayerPosition, resetSolvingSteps } = useContext(mazeContext);
    const { menuState, setAnimationSpeed, setAlgorithm, toggleEditingMode, toggleInProgress } = useContext(menuContext);

    const animatePlayer = () => {
        toggleInProgress();
        mazeState.solvingSteps.forEach((step, i) => {
            if (i === mazeState.solvingSteps.length - 1) {
                setTimeout(() => {
                    setPlayerPosition(step);
                    toggleInProgress();
                }, (150 - menuState.animationSpeed) * i);

            } else {
                setTimeout(() => {
                    setPlayerPosition(step);
                }, (150 - menuState.animationSpeed) * i);
            }
        });
    }
    
    useEffect(() => {
        if (mazeState.solvingSteps.length > 10) {
            animatePlayer();
        }
    }, [mazeState.solvingSteps])

    function importImagesFromDir(r) {
        let images = [];
        r.keys().map((item, index) =>
            images.push((images[item.replace('./', '')] = r(item)).default));
        return images;
    }
    const agentImage = importImagesFromDir(require.context('./../../images/characters', false, /\.(png|jpe?g|svg)$/))[mazeState.agentIndex];

    return (
        <div id="lm-container">
            <div id="lm-mode-selection">
                <p style={{ marginBottom: '0.75rem' }}>Modus</p>
                <ToggleButtonGroup
                    value={menuState.algorithm}
                    exclusive
                    onChange={(e, value) => value !== null ? setAlgorithm(value) : null}
                    aria-label="algorithm"
                >
                    <ToggleButton disabled={menuState.inProgress} value="PLEDGE" className="toglbtn">
                        <Tooltip title="Pledge" TransitionComponent={Zoom} arrow>
                            <div>
                                <MazeIcon />
                            </div>
                        </Tooltip>
                    </ToggleButton>
                    <ToggleButton disabled={menuState.inProgress} value="RIGHT_HAND" className="toglbtn">
                        <Tooltip title="Hand" TransitionComponent={Zoom} arrow>
                            <PanToolIcon style={{ fontSize: '1.15rem' }} />
                        </Tooltip>
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
            <div id="lm-toolbox-selection">
                <p style={{ marginBottom: '0.75rem' }}>Bearbeitung</p>
                <div style={{ display: 'flex' }}>
                    <Button variant="contained" disabled={menuState.inProgress} id="lm-editing-btn" onClick={toggleEditingMode} className={menuState.editingMode ? 'lm-editing' : 'lm-not-editing'}>
                        <Tooltip title="Pen" TransitionComponent={Zoom} arrow>
                            <CreateIcon />
                        </Tooltip>
                    </Button>
                </div>
            </div>
            <div id="lm-animation-speed">
                <p style={{ marginBottom: '0.75rem' }}>Animationsgeschwindigkeit</p>
                <CustomSlider
                    value={menuState.animationSpeed}
                    onChange={(e, value) => value !== null ? setAnimationSpeed(value) : null}
                    aria-labelledby="animation-slider"
                    valueLabelDisplay="auto"
                    disabled={menuState.inProgress}
                    style={{ width: '100%', color: 'grey' }}
                    step={10}
                    min={10}
                    max={100}
                />
            </div>

            {
                menuState.inProgress &&
                <div id="lm-live-stats">
                    <div id="live-stats-view">
                        <div id="lm-live-agent-view">
                            <img src={agentImage} height="40" width="40" style={{ transform: `rotate(${mazeState.playerDir}deg)` }} />
                        </div>
                        <div id="lm-live-number-stats">
                            <p style={{ fontWeight: 'bold' }}>Drehung: </p>
                            <p style={{ fontWeight: 'bold', fontSize: '1.25rem' }}>{mazeState.playerDir}°</p>
                        </div>
                        <CircularProgress id="lm-live-stat-indicator" />
                    </div>
                </div>
            }
            
            {
                !menuState.inProgress &&
                <div id="lm-controls">
                    <div id="lm-move-cluster">
                        <div id="lm-move-container">
                            <p style={{ marginBottom: '0.75rem' }}>Manuell bewegen</p>
                            <div id="lm-move">
                                <Fab
                                    id="toggle-btn-fab"
                                    disabled={menuState.inProgress}
                                    style={{
                                        borderRadius: '5px 0 0 5px',
                                        margin: '1px',
                                    }}
                                    onClick={() => turnPlayer('LEFT')}
                                >
                                    <RotateLeftIcon />
                                </Fab>
                                <Fab
                                    id="toggle-btn-fab"
                                    disabled={menuState.inProgress}
                                    style={{
                                        borderRadius: '5px',
                                        margin: '1px',
                                    }}
                                    onClick={() => movePlayerForward()}
                                >
                                    <KeyboardArrowUpIcon />
                                </Fab>
                                <Fab
                                    id="toggle-btn-fab"
                                    disabled={menuState.inProgress}
                                    style={{
                                        borderRadius: '0 5px 5px 0',
                                        margin: '1px',
                                    }}
                                    onClick={() => turnPlayer('RIGHT')}
                                >
                                    <RotateRightIcon />
                                </Fab>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {
                !menuState.inProgress &&
                <div id="lm-move-container">
                    <p style={{ marginBottom: '0.75rem' }}>Wegfinden</p>
                    <div id="lm-options-cluster">
                        <Fab
                            id="toggle-btn-fab"
                            style={{ borderRadius: '5px 0 0 5px' }}
                            onClick={() => {
                                if (mazeState.playerPos[0] === mazeState.endPos[0] && mazeState.playerPos[1] === mazeState.endPos[1]) {
                                    animatePlayer();
                                } else {
                                    solveMaze(menuState.algorithm);
                                }
                            }}
                            disabled={menuState.editingMode}
                            className={menuState.editingMode ? 'disabled-toggle-btn' : null}
                        >
                            <PlayArrowIcon />
                        </Fab>
                    </div>
                </div>
            }
        </div>
    );
};

export default LabyrinthMenu;
