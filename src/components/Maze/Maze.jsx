import { SettingsOverscanTwoTone } from '@material-ui/icons';
import React, { useContext, useState } from 'react';

import { mazeContext } from '../../context/MazeProvider';
import { menuContext } from '../../context/MenuProvider';

import './maze-style.css';

const Maze = () => {
    const { mazeState, setMaze } = useContext(mazeContext);
    const { menuState } = useContext(menuContext); 
    // current tool for drag and drop
    const [ currentTool, setCurentTool] = useState("pen");

    // edit a single field in the maze with a single click
    const editMazeField = (rowIndex, columnIndex, type) => {
        if(menuState.editingMode) {
            setCurentTool(type);
            let currentMaze = mazeState.maze;
            if(type === 'ERASER') currentMaze[rowIndex][columnIndex] = 0;
            if(type === 'PEN') currentMaze[rowIndex][columnIndex] = 1;
            setMaze(currentMaze);
        }
    }

    // edit an area in the maze with drag and drop

    const editMazeArea = (rowIndex, columnIndex) => {
        if(menuState.editingMode) {
            let currentMaze = mazeState.maze;
            if(currentTool === 'ERASER') currentMaze[rowIndex][columnIndex] = 0;
            if(currentTool === 'PEN') currentMaze[rowIndex][columnIndex] = 1;
            setMaze(currentMaze);
        }
    }

    const importImagesFromDir = (r) => {
        let images = [];
        r.keys().map((item, index) => 
            images.push((images[item.replace('./', '')] = r(item)).default));
        return images;
    } 

    const images = importImagesFromDir(require.context('./../../images/characters', false, /\.(png|jpe?g|svg)$/));

    return (
        <div id="mz-container" style={{ color: 'white' }}>
            <div id="mz-table-container">
                <table className="mz-table">
                    <tbody>
                        {mazeState.maze.map((row, rowIndex) => (
                            <tr key={rowIndex} className="maze-row">
                                {row.map((column, columnIndex) => (
                                    <td key={columnIndex} 
                                        className="maze-column" 
                                        onContextMenu={() => editMazeField(rowIndex, columnIndex, 'ERASER')} 
                                        onClick={() => editMazeField(rowIndex, columnIndex, 'PEN')}  
                                        onDragOver={ () => editMazeArea(rowIndex, columnIndex)}
                                        draggable
                                    >
                                        <div
                                            className={
                                                column === 1
                                                    ? 'maze-wall'
                                                    : mazeState.playerPos[0] === rowIndex &&
                                                        mazeState.playerPos[1] === columnIndex
                                                        ? 'maze-player'
                                                        : mazeState.startPos[0] === rowIndex &&
                                                            mazeState.startPos[1] === columnIndex
                                                            ? 'maze-start'
                                                            : mazeState.endPos[0] === rowIndex &&
                                                                mazeState.endPos[1] === columnIndex
                                                                ? 'maze-end'
                                                                : ''
                                            }
                                        >
                                            {
                                                (mazeState.startPos[0] === rowIndex && mazeState.startPos[1] === columnIndex) &&
                                                <div id="indicator-letter">
                                                    S
                                                </div>
                                            }

                                            {
                                                (mazeState.endPos[0] === rowIndex && mazeState.endPos[1] === columnIndex) &&
                                                <div id="indicator-letter">
                                                    Z
                                                </div>
                                            }

                                            {
                                                (mazeState.playerPos[1] === columnIndex && mazeState.playerPos[0] === rowIndex) &&
                                                <div id="character-wrapper" style={{ transform: `rotate(${mazeState.playerDir}deg)` }}>
                                                    <img src={images[mazeState.agentIndex]} id="mz-agent-image" alt="character" />
                                                </div>
                                            }
                                                
                                        </div>
                                    </td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default Maze;
