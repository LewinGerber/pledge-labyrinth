import React, { useContext } from 'react';

import SideMenu from '../SideMenu/SideMenu';
import LabyrinthMenu from '../LabyrinthMenu/LabyrinthMenu';
import GridDisplay from '../GridDisplay/GridDisplay';
import Maze from '../Maze/Maze';

import { menuContext } from '../../context/MenuProvider'

import './app-style.css';

const App = () => {
    const { menuState, setAnimationSpeed } = useContext(menuContext);

    // checks whether a number is positive or not
    const isPositive = (num) => {
        return num > 0;
    };

    // changes the animation speed with the mouse wheel
    const speedWheel = (e) => {
        let currentSpeed = menuState.animationSpeed;
        if (isPositive(e.nativeEvent.wheelDelta)) {
            if (currentSpeed < 100) setAnimationSpeed(currentSpeed + 10);
        } else {
            if (currentSpeed > 0) setAnimationSpeed(currentSpeed - 10);
        }
    };

    return (
        <div className="app">
            <div id="vert-splitter">
                <SideMenu />
                <div id="top-splitter" >
                    <GridDisplay>
                        <Maze />
                    </GridDisplay>
                    <div onWheel={speedWheel} style={{ width: '100%', height: '100px' }}>
                        <LabyrinthMenu />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;
