import React from 'react';

import './grid-display-style.css';

const GridDisplay = (props) => {
    return (
        <div id="gd-container">
            <div id="gd-display-area">{props.children}</div>
        </div>
    );
};

export default GridDisplay;
