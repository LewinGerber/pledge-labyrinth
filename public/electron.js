const electron = require('electron');

const { app } = electron;
const { BrowserWindow } = electron;

const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({
         width: 1010,
         minWidth: 1010, 
         height: 570, 
         minHeight: 570,
         icon: 'public/favicon.ico',
         webSecurity: false,
         center: true,
         frame: false, 
         transparent: true, 
         titleBarStyle: 'hiddenInset', 
         webPreferences: { 
             nodeIntegration: true,
             enableRemoteModule: true 
        }
    }),
    //mainWindow.webContents.openDevTools({mode:'undocked'});
    mainWindow.loadURL(
        isDev
            ? 'http://localhost:3000'
            : `file://${path.join(__dirname, '../build/index.html')}`
    );
    mainWindow.on('closed', () => {
        mainWindow = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});